-module(hello).
-export([hello/0,add/2,greet_and_add_two/1,help_me/1]).
-auther("SomeBody").

add(A,B) ->
    A+B.

greet_and_add_two(X) ->
    hello(),
    add(X,2).

second([_,X|_]) ->
    X.
%% note, this one would be better as a pattern match in function heads!
%% I'm doing it this way for the sake of the example.
help_me(Animal) ->
    Talk = if Animal =:= cat  -> "meow";
              Animal =:= beef -> "mooo";
              Animal =:= dog  -> "bark";
              Animal =:= tree -> "bark";
              true -> "fgdadfgna"
           end,
    {Animal, "says " ++ Talk ++ "!"}.

%% Only this function is exported
hello() ->    % No output for empty list
    %% io:format("~s~n",[<<"Hello">>]), io:format("~p~n",[<<"Hello">>]), io:format("~~~n"), io:format("~f~n", [4.0]), io:format("~30f~n", [4.0]).
    second([1,2]),
    io:format("Hello, world!~n").
