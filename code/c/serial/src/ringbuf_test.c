#include "ringbuf.h"
#include <stdio.h>
#include <stdlib.h>

ringbuf_t g_ringbuf;
uint8_t ringbuf_cache[1024];

int main(int argc, char **argv)
{
    ringbuf_t * p_ringbuf = &g_ringbuf;
    ringbuf_init(p_ringbuf, ringbuf_cache, sizeof(ringbuf_cache));
    printf("full=%d,empty=%d,used=%d,free=%d\n", 
            ringbuf_is_full(p_ringbuf),
            ringbuf_is_empty(p_ringbuf),
            ringbuf_bytes_used(p_ringbuf),
            ringbuf_bytes_free(p_ringbuf));

    return 0;
}

