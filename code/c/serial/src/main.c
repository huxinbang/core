#include "tc_iot_at_driver.h"
#include <pthread.h>

tc_iot_at_device g_at_device;

pthread_t thread;

void * fn(void *arg)
{
    tc_iot_at_device * p_at_device = arg;
    ringbuf_t * p_ringbuf = &(p_at_device->ringbuf);

    for (;;) {

        if (!ringbuf_is_full(p_ringbuf)) {
            ringbuf_read(p_at_device->dev_fd, p_ringbuf, ringbuf_bytes_free(p_ringbuf));
            /* printf("%d counts read\n", nread); */
        } else {
            sleep_ms(10);
        }
    }
    return arg;
}

/**
 *@breif 	main()
 */
int main(int argc, char **argv)
{
    int ret;
    char buff[1024];
    const char * test_data = "Hello, I am here.~!@#$%^&*()_+`1234567890qwertyuiop[]\\asdfghjkl;'zxcvbnm,./'`";
    int retry_times = 0;
    const char * host = "193.112.55.94";
    int port = 8000;
    tc_iot_at_dev_context context = {"/dev/ttyXRUSB0"};
    tc_iot_at_device * p_at_device = &g_at_device;

    at_init(p_at_device, &context);

    pthread_create(&thread, NULL, fn, p_at_device);

query_status:
    at_send(p_at_device, AT_CSQ, sizeof(AT_CSQ)-1, AT_SEND_TIMEOUT_MS);

    ret = at_expect(p_at_device, AT_OK, AT_ERROR,  buff, sizeof(buff), NET_REQUEST_TIMEOUT_MS);
    if (ret > 0) {
        if (strstr(buff, "+CSQ:99,99") != NULL) {
            retry_times++;
            printf("communication module signal not ready retry %d times\n", retry_times);
            sleep(1);
            goto query_status;
        } else {
            printf(">Query success\n");
            retry_times = 0;
        }
    } else {
        printf(">Query Failed, restarting communication module\n");
    reset_module:
        at_send(p_at_device, AT_RST,sizeof(AT_RST)-1, AT_RESET_TIMEOUT_MS);

        ret = at_expect(p_at_device, AT_OK, AT_ERROR,  buff, sizeof(buff), NET_REQUEST_TIMEOUT_MS);
        if (ret > 0) {
            printf(">System Start Ready\n");
            goto query_status;
        } else {
            printf(">System Start Failed\n");
        }
    }

    at_send(p_at_device, AT_UDP_SOCKET,sizeof(AT_UDP_SOCKET)-1,AT_SEND_TIMEOUT_MS);
    ret = at_expect(p_at_device, AT_OK, AT_ERROR,  buff, sizeof(buff), NET_REQUEST_TIMEOUT_MS);
    if (ret > 0) {
        printf(">Socket create success:\n");
    } else {
        printf(">Socket create failed\n");
    }
    sleep_ms(200);

    retry_times = 0;
send_data:
    at_udp_request(p_at_device, host, port, test_data, strlen(test_data));
    ret = at_expect(p_at_device, AT_OK, AT_ERROR,  buff, sizeof(buff), NET_REQUEST_TIMEOUT_MS);
    if (ret > 0) {
        printf(">send data success\n");
    } else {
        printf(">send data failed\n");
    }

    ret = at_expect(p_at_device, AT_PACK_ARRIVE_INDICATOR, AT_ERROR, buff, sizeof(buff), 10000);
    if (ret > 0) {
        ret = at_expect( p_at_device, AT_EOF, AT_ERROR, buff, sizeof(buff), 10000);
        if (ret > 0) {
            printf(">new data received\n");
        } else {
            printf(">invalid response\n");
        }
    } else {
        if (retry_times >= 5) {
            printf(">send_data retry_times = %d, resetting module.\n", retry_times);
            goto reset_module;
        } else {
            retry_times++;
            goto send_data;
        }

        printf("\n>no response\n");
    }


    ret = at_udp_receive(p_at_device, buff, sizeof(buff));
    if (ret > 0) {
        printf(">resp data:%s\n", buff);
    } else {
        printf(">receive failed:ret=%d\n", ret);
    }

    at_close_socket(p_at_device, 1);
    ret = at_expect(p_at_device, AT_OK, AT_ERROR,  buff, sizeof(buff), NET_REQUEST_TIMEOUT_MS);
    if (ret > 0) {
        printf(">close socket success\n");
    } else {
        printf(">close socket failed\n");
    }

    exit(0);
}

