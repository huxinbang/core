#include "ringbuf.h"

int ringbuf_init(ringbuf_t * rb, uint8_t * buffer, int buffer_len)
{
    if (rb) {
        rb->size = buffer_len;
        rb->buf = buffer;
        if (rb->buf)
            ringbuf_reset(rb);
        else {
            return 0;
        }
    }
    return 0;
}

int32_t ringbuf_buffer_size(const ringbuf_t *rb) {
    return rb->size;
}

void ringbuf_reset(ringbuf_t * rb) {
    rb->head = rb->tail = rb->buf;
}

void ringbuf_free(ringbuf_t * rb)
{
    rb->head = rb->tail = rb->buf = NULL;
    rb->size = 0;
}

int32_t ringbuf_capacity(const ringbuf_t *rb)
{
    return ringbuf_buffer_size(rb) - 1;
}

static const uint8_t * ringbuf_end(const ringbuf_t *rb)
{
    return rb->buf + ringbuf_buffer_size(rb);
}


int32_t ringbuf_bytes_free(const ringbuf_t *rb)
{
    if (rb->head >= rb->tail) {
        return ringbuf_capacity(rb) - (rb->head - rb->tail);
    } else{
        return rb->tail - rb->head - 1;
    }
}

int32_t ringbuf_bytes_used(const ringbuf_t *rb)
{
    return ringbuf_capacity(rb) - ringbuf_bytes_free(rb);
}

int ringbuf_is_full(const ringbuf_t *rb)
{
    return ringbuf_bytes_free(rb) == 0;
}

int ringbuf_is_empty(const ringbuf_t *rb)
{
    return ringbuf_bytes_free(rb) == ringbuf_capacity(rb);
}

const void * ringbuf_tail(const ringbuf_t *rb)
{
    return rb->tail;
}

const void * ringbuf_head(const ringbuf_t *rb)
{
    return rb->head;
}

static uint8_t * ringbuf_nextp(ringbuf_t * rb, const uint8_t *p)
{
    assert((p >= rb->buf) && (p < ringbuf_end(rb)));
    return rb->buf + ((++p - rb->buf) % ringbuf_buffer_size(rb));
}

int32_t ringbuf_findchr(const ringbuf_t *rb, int c, int32_t offset)
{
    const uint8_t *bufend = ringbuf_end(rb);
    int32_t bytes_used = ringbuf_bytes_used(rb);
    if (offset >= bytes_used) {
        return bytes_used;
    }

    const uint8_t *start = rb->buf +
        (((rb->tail - rb->buf) + offset) % ringbuf_buffer_size(rb));
    assert(bufend > start);
    int32_t n = MIN(bufend - start, bytes_used - offset);
    const uint8_t *found = memchr(start, c, n);
    if (found)
        return offset + (found - start);
    else
        return ringbuf_findchr(rb, c, offset + n);
}

int32_t ringbuf_memset(ringbuf_t * dst, int c, int32_t len)
{
    const uint8_t *bufend = ringbuf_end(dst);
    int32_t nwritten = 0;
    int32_t count = MIN(len, ringbuf_buffer_size(dst));
    int overflow = count > ringbuf_bytes_free(dst);

    while (nwritten != count) {

        /* don't copy beyond the end of the buffer */
        assert(bufend > dst->head);
        int32_t n = MIN(bufend - dst->head, count - nwritten);
        memset(dst->head, c, n);
        dst->head += n;
        nwritten += n;

        /* wrap? */
        if (dst->head == bufend)
            dst->head = dst->buf;
    }

    if (overflow) {
        dst->tail = ringbuf_nextp(dst, dst->head);
        assert(ringbuf_is_full(dst));
    }

    return nwritten;
}

void * ringbuf_memcpy_into(ringbuf_t * dst, const void *src, int32_t count)
{
    const uint8_t *u8src = src;
    const uint8_t *bufend = ringbuf_end(dst);
    int overflow = count > ringbuf_bytes_free(dst);
    int32_t nread = 0;

    while (nread != count) {
        /* don't copy beyond the end of the buffer */
        assert(bufend > dst->head);
        int32_t n = MIN(bufend - dst->head, count - nread);
        memcpy(dst->head, u8src + nread, n);
        dst->head += n;
        nread += n;

        /* wrap? */
        if (dst->head == bufend)
            dst->head = dst->buf;
    }

    if (overflow) {
        dst->tail = ringbuf_nextp(dst, dst->head);
        assert(ringbuf_is_full(dst));
    }

    return dst->head;
}


int32_t ringbuf_read(int fd, ringbuf_t * rb, int32_t count)
{
    const uint8_t *bufend = ringbuf_end(rb);
    int32_t nfree = ringbuf_bytes_free(rb);

    /* don't write beyond the end of the buffer */
    assert(bufend > rb->head);
    count = MIN(bufend - rb->head, count);
    int32_t n = read(fd, rb->head, count);
    if (n > 0) {
        assert(rb->head + n <= bufend);
        rb->head += n;

        /* wrap? */
        if (rb->head == bufend)
            rb->head = rb->buf;

        /* fix up the tail pointer if an overflow occurred */
        if (n > nfree) {
            rb->tail = ringbuf_nextp(rb, rb->head);
            assert(ringbuf_is_full(rb));
        }
    }

    return n;
}

void * ringbuf_memcpy_from(void *dst, ringbuf_t * src, int32_t count)
{
    int32_t bytes_used = ringbuf_bytes_used(src);
    if (count > bytes_used) {
        return 0;
    }

    uint8_t *u8dst = dst;
    const uint8_t *bufend = ringbuf_end(src);
    int32_t nwritten = 0;
    while (nwritten != count) {
        assert(bufend > src->tail);
        int32_t n = MIN(bufend - src->tail, count - nwritten);
        memcpy(u8dst + nwritten, src->tail, n);
        src->tail += n;
        nwritten += n;

        /* wrap ? */
        if (src->tail == bufend) {
            src->tail = src->buf;
        }
    }

    assert(count + ringbuf_bytes_used(src) == bytes_used);
    return src->tail;
}

int32_t ringbuf_write(int fd, ringbuf_t * rb, int32_t count)
{
    int32_t bytes_used = ringbuf_bytes_used(rb);
    if (count > bytes_used) {
        return 0;
    }

    const uint8_t *bufend = ringbuf_end(rb);
    assert(bufend > rb->head);
    count = MIN(bufend - rb->tail, count);
    int32_t n = write(fd, rb->tail, count);
    if (n > 0) {
        assert(rb->tail + n <= bufend);
        rb->tail += n;

        /* wrap? */
        if (rb->tail == bufend)
            rb->tail = rb->buf;

        assert(n + ringbuf_bytes_used(rb) == bytes_used);
    }

    return n;
}


